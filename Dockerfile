# Stage 1
FROM node:19-alpine as build-step
RUN apk add git
RUN mkdir -p /app
WORKDIR /app
COPY package.json  /app
COPY /nginx/nginx.conf /app
RUN yarn install
COPY . /app
RUN yarn build

# Stage 2
FROM nginx:1.17.1-alpine
COPY --from=build-step /app/dist/front-end /usr/share/nginx/html
COPY --from=build-step /app/nginx.conf /etc/nginx/conf.d/comicbox.conf
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]
