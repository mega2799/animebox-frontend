import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './components/auth/login/login.component';
import { RegisterComponent } from './components/auth/register/register.component';
import { MangaComponent } from './components/manga/manga.component';
import { HomeComponent } from './components/home/home.component';
import { CustomersComponent } from './components/customers/customers.component';
import { BoxComponent } from './components/box/box.component';

const routes: Routes = [
    {path:"",pathMatch:"full",component:HomeComponent},  
    {path:"login",component:LoginComponent},
    {path:"register",component:RegisterComponent},
    {path:"manga", component:MangaComponent},
    {path:"customers", component:CustomersComponent},
    {path:"box/:CF", component:BoxComponent}
]

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
  })
  export class AppRoutingModule { }