import { CommonModule } from '@angular/common';
import { Component, Inject } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MAT_DIALOG_DATA, MatDialogModule } from '@angular/material/dialog';
import { MatDivider, MatDividerModule } from '@angular/material/divider';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';


@Component({
  selector: 'dialog-content-example-dialog',
  templateUrl: './manga-io.dialog.html',
  standalone: true,
  imports: [MatDialogModule, MatButtonModule, CommonModule, MatFormFieldModule, MatInputModule, FormsModule, MatDividerModule],
})
export class DialogContentManga {
  constructor(@Inject(MAT_DIALOG_DATA) public data: any) {}

  showOnlyMangaInfo = false;
  
}
