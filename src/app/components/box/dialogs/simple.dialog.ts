import { CommonModule } from '@angular/common';
import { Component, Inject } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MAT_DIALOG_DATA, MatDialogModule } from '@angular/material/dialog';




@Component({
  selector: 'dialog-example-dialog',
  templateUrl: './simple.dialog.html',
  standalone: true,
  imports: [MatDialogModule, MatButtonModule, CommonModule],
})
export default class SimpleDialog {
  constructor(@Inject(MAT_DIALOG_DATA) public data: any) { }
}
