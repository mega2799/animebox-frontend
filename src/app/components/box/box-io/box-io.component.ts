import { NgFor, AsyncPipe, CommonModule } from '@angular/common';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormControl, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatButtonModule } from '@angular/material/button';
import { MatDividerModule } from '@angular/material/divider';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { ButtonIOComponent } from '../../button-io/button-io.component';
import { MangaService } from 'src/app/services/manga.service';
import { MatDialog, MatDialogModule } from '@angular/material/dialog';
import { DialogContentManga } from '../dialogs/contentManga.dialog.';
import { AddMangaDialog } from '../dialogs/add-manga.dialog';
import SimpleDialog from '../dialogs/simple.dialog';
import { DialogRef } from '@angular/cdk/dialog';
import { BoxService } from 'src/app/services/box.service';


export interface Volume {
  readonly number: number;
  readonly released: Date;
}

export interface Manga {

  readonly title: string;

  readonly description: string;

  readonly image: string;

  readonly genres: string[];

  readonly status: 'Ongoing' | 'Completed';

  readonly rating: number;

  readonly authors: string[];

  readonly volumes: Volume[];
}

@Component({
  selector: 'app-box-io',
  templateUrl: '../../button-io/button-io.component.html',
  styleUrls: ['../../button-io/button-io.component.css'],
  // styleUrls: ['./manga-io.component.css'],
  standalone: true,
  imports: [MatButtonModule, MatDividerModule, MatIconModule,
    FormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatAutocompleteModule,
    ReactiveFormsModule,
    NgFor,
    MatSlideToggleModule,
    AsyncPipe,
    CommonModule,
    MatDialogModule
  ],
})
export class BoxIoComponent extends ButtonIOComponent implements OnInit {

  constructor(private readonly mangaService: MangaService,
    public dialog: MatDialog,
    private readonly boxService: BoxService,
  ) {
    super()
  }

  override displayAdd: boolean = false;


  @Output() event = new EventEmitter<any>();

  @Input()
  override addIcon: string = 'note_add'
  @Input()
  boxId: string = '';

  override label: string = 'Manga'
  override subTitleBar: string = ''

  ngOnInit(): void {
    this.mangaService.getMangas().subscribe(resp => {
      const r: any[] = resp;
      // this.listOfEntities = r.map((entity) => { return { flag: entity.image, name: entity.title,
      //   population: new Date(Math.max(...entity.volumes.map((vol: { released: any; }) => Date.parse(vol.released)))).toLocaleDateString() } })
      this.listOfEntities = r.map((entity) => {
        const latestVolumeTimestamp = Math.max(...entity.volumes.map((vol: { released: any; }) => Date.parse(vol.released)));
        const latestVolumeId = entity.volumes.find((vol: { released: any; number : number }) => Date.parse(vol.released) === latestVolumeTimestamp).number
        return { flag: entity.image, name: entity.title,
        littleTitle: latestVolumeId + ' on ' + new Date(Math.max(...entity.volumes.map((vol: { released: any; }) => Date.parse(vol.released)))).toLocaleDateString(), 
        latestNum : latestVolumeId 
    } })
    });
        
    this.stateCtrl.valueChanges.forEach((val) => console.log(val))
  }


  //TODO non va bene unire le cose, quindi l add manga avra' il suo bar in sui scrivere
  // e aggiungere eventualmente la foto
  // override addEntity() {
  //   if (this.stateCtrl.value) {
  //     const selectedManga: string = this.stateCtrl.value;

  //     if (this.listOfEntities.filter((entity) => entity.name === selectedManga).length) {
  //       this.dialog.open(SimpleDialog, {
  //         data: {
  //           message: `There's already !`
  //         }
  //       })
  //       return
  //     }
  //     const dialogRef = this.dialog.open(AddMangaDialog)

  //     dialogRef.afterClosed().subscribe(imageSrc => {
  //       this.mangaService.addManga({ title: selectedManga, image: imageSrc }).subscribe((res: any) => {
  //         if (!res) console.log('Non si e trovato');
  //         window.location.reload();
  //       });

  //     })

  //     // this.mangaService.addManga(selectedManga).subscribe((res : any) => {
  //     //   if(!res) console.log('Non si e trovato');
  //     // });
  //   }
  // }

  override openDetail() {
    if (this.stateCtrl.value) {
      const selectedManga: string = this.stateCtrl.value;

      if (!this.listOfEntities.filter((entity) => entity.name === selectedManga).length) {
        this.dialog.open(SimpleDialog, {
          data: {
            message: `There's no ${selectedManga}!`
          }
        })
        return
      }
      this.mangaService.getManga(selectedManga).subscribe((res: any) => {
        // const latestVolumeTimestamp = Math.max(...res.volumes.map((vol: { released: any; }) => Date.parse(vol.released)));
        // const latestVolumeId = res.volumes.find((vol: { released: any; number : number }) => Date.parse(vol.released) === latestVolumeTimestamp).number
        
        const dialogRef = this.dialog.open(DialogContentManga, {
          data: {
            res,
            currentId: 0,
            // latest: latestVolumeId,
          }
        });

        dialogRef.afterClosed().subscribe(resp => {
          this.boxService.updateBox(this.boxId, {
            currentID: Number(resp.currentId),
            product: {
              title: resp.res.title,
              image: resp.res.image
            }
          }).subscribe(res => { 
            this.event.emit(true);
          })
        })
      });


    } else {
      console.log(`None selected`);

    }
  }

}


