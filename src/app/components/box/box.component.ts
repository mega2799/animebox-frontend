import { KeyValue } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { BoxService } from 'src/app/services/box.service';
import { CustomerService } from 'src/app/services/customer.service';
import { Manga } from './box-io/box-io.component';
import { MangaService } from 'src/app/services/manga.service';
import { MatMiniFabAnchor } from '@angular/material/button';
import { MatDialog } from '@angular/material/dialog';
import SimpleDialog from './dialogs/simple.dialog';

@Component({
  selector: 'app-box',
  templateUrl: './box.component.html',
  styleUrls: ['./box.component.css']
})
export class BoxComponent implements OnInit {
onAddiction(event: Event) {
  console.log(event);
  if(event){
    this.renderMangas();
  }
  
}
  cf: any | string;
  box: any;
  customer: any;
  // customerForm : any; 
  // customerForm : FormGroup = new FormGroup('');
  customerForm: FormGroup = new FormGroup({
    _id: new FormControl(''),
    CF: new FormControl(''),
    name: new FormControl(''),
    surname: new FormControl(''),
    email: new FormControl(''),
    telegramID: new FormControl(''),
    image :  new FormControl(''),
  });


  constructor(private route: ActivatedRoute,
    private customerService: CustomerService,
    private boxService: BoxService,
    private mangaService: MangaService,
    public dialog: MatDialog,
  ) { }

  customerView = (a: KeyValue<string, string>, b: KeyValue<string, string>): number => {
    return 0
  }
  customFields: string[] = ['__v', 'image'];
  listOfEntities: any = []

  renderMangas() {
      this.boxService.getPersonalBox(this.customer._id).subscribe(resp => {
        if (!resp) {
          this.boxService.createBox(this.customer._id).subscribe(res => {
            this.box = resp;
            window.location.reload()
          })
        } else {
          this.box = resp;
          this.box.customerProducts = resp.customerProducts.map((cProd: any) => { return { ...cProd, latest: this.listOfEntities.find((manga: any) => manga.name === cProd.product.title).latestNum } })
        }
      })
  }

  renderUserInfo(){
    this.cf = this.route.snapshot.paramMap.get('CF');
    this.customerService.getCustomer(this.cf).subscribe(res => {
      this.customer = res
      this.customerForm.patchValue(this.customer)
    })
  }

  ngOnInit(): void {
    // setInterval(() => {
    this.mangaService.getMangas().subscribe(resp => {
      const r: any[] = resp;
      // this.listOfEntities = r.map((entity) => { return { flag: entity.image, name: entity.title,
      //   population: new Date(Math.max(...entity.volumes.map((vol: { released: any; }) => Date.parse(vol.released)))).toLocaleDateString() } })
      this.listOfEntities = r.map((entity) => {
        const latestVolumeTimestamp = Math.max(...entity.volumes.map((vol: { released: any; }) => Date.parse(vol.released)));
        const latestVolumeId = entity.volumes.find((vol: { released: any; number: number }) => Date.parse(vol.released) === latestVolumeTimestamp).number
        return {
          flag: entity.image, name: entity.title,
          latest: (new Date(Math.max(...entity.volumes.map((vol: { released: any; }) => Date.parse(vol.released)))).toLocaleDateString()),
          latestNum: latestVolumeId
        }
      })
    });

    this.cf = this.route.snapshot.paramMap.get('CF');
    this.customerService.getCustomer(this.cf).subscribe(res => {
      this.customer = res
      this.customerForm.patchValue(this.customer)
      this.renderMangas()
    })
    //   this.boxService.getPersonalBox(this.customer._id).subscribe(resp => {
    //     if (!resp) {
    //       this.boxService.createBox(this.customer._id).subscribe(res => {
    //         this.box = resp;
    //         window.location.reload()
    //       })
    //     } else {
    //       this.box = resp;
    //       this.box.customerProducts = resp.customerProducts.map((cProd: any) => { return { ...cProd, latest: this.listOfEntities.find((manga: any) => manga.name === cProd.product.title).latestNum } })
    //     }
    //   })
    // })
  }

  onSubmit() {
    console.log(this.customerForm.value); 
    this.customerService.updateCustomer(this.customerForm.value).subscribe(res => {
      this.renderUserInfo();
    })

  }

  advancedVolume(boxId : string, title: number){
    console.log(boxId, title);
    const current = this.box.customerProducts.find((cProd : any) => cProd.product.title === title)
    if(current.currentID < current.latest){
    this.boxService.increaseStatus(boxId, title).subscribe(res => {
      this.renderMangas();
    })
    }else{
      this.dialog.open(SimpleDialog, {
        data: {
          message: `This is the latest Volume, wait ヾ(￣◇￣)ノ〃`
        }
      })
    }
  }


  deleteVolume(boxId : string, title: number, currentID : number){
    console.log(boxId, title, currentID);
    this.boxService.deleteVolume(boxId, title, currentID).subscribe(res => {
      this.renderMangas();
    })
  }

  //   trackByMethod(index:number, el:any): number {
  //     return el.id;
  // }
}
