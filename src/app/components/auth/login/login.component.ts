import { HttpClient } from '@angular/common/http';
import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, ValidationErrors } from '@angular/forms';
import { environment } from 'src/environments/environment';
import { ProgressSpinnerMode, MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatSliderModule } from '@angular/material/slider';
import { sha512 } from 'js-sha512';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent {
  form: FormGroup;
  loading: boolean = false;
  @Output() user = new EventEmitter<any>();
  logged: boolean = JSON.parse(localStorage.getItem('loggedIn') || 'false');
  title: string = 'Register'; // localStorage.getItem('format') || ''
  error : string ='';
  constructor(public fb: FormBuilder, private http: HttpClient) {
    this.form = this.fb.group({
      username: [''],
      password: [null],
    });
  }

  isLoggedIn() {
    return JSON.parse(localStorage.getItem('loggedIn') || this.logged.toString());
  }

  getVal(value: ValidationErrors | null) {
    if(value != null){
      if(value['unauthenticated'] === true){
        this.error = 'Username or Password incorrect!'
        return true
      }
      if(value['unregistered'] === true){
        this.error = 'Username is already taken!'
        return true
      }
    }
    return false;
  }
  submitForm() {
    console.log(this.form.value);

    if (!this.form.get('username')?.value || !this.form.get('password')?.value) {
      this.title = this.title === 'Register' ? 'Login' : 'Register'
    } else {
      let datas: any = {}
      datas.username = this.form.get('username')?.value;
      datas.password = sha512(this.form.get('password')?.value);
      if (this.title === 'Register') {
        this.http
          .post(environment.API + 'auth', datas, {
            // .post('http://localhost:90/api/'+ 'auth/login', datas, {
          })
          .subscribe({
            next: (response: any) => {
              window.location.reload();
            },
            error: (error) => { console.log(error),this.form.setErrors({ unregistered : true })  },
          });
      } else {
        this.http
          .post(environment.API + 'auth/login', datas, {
            // .post('http://localhost:90/api/'+ 'auth/login', datas, {
          })
          .subscribe({
            next: (response: any) => {
              this.logged = true, this.user.emit(response.username), console.log('Emitted'),
                this.loading = true;
              localStorage.setItem('loggedIn', 'true');
              localStorage.setItem('username', `${response.username}`),
                window.location.reload();
            },
            error: (error) => {
              console.log(error), this.form.setErrors({ unauthenticated: true }),
              console.log(this.form.errors);
            },
          });
      }
    }
  }
}
