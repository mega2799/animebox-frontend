import { HttpClient } from '@angular/common/http';
import { Component, EventEmitter, Output } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { sha512 } from 'js-sha512';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent {
  form: FormGroup;
  loading : boolean = false;
  @Output() user = new EventEmitter<any>();
  // logged : boolean = JSON.parse(localStorage.getItem('loggedIn') || 'false');
  constructor(public fb: FormBuilder, private http: HttpClient, private router : Router) {
    this.form = this.fb.group({
      username: [''],
      password: [null],
    });
  }

   submitForm() {
    let datas : any = {} 
    datas.username = this.form.get('username')?.value;
    datas.password = sha512(this.form.get('password')?.value);
    this.http
      .post(environment.API + 'auth', datas, {
      // .post('http://localhost:90/api/'+ 'auth/login', datas, {
      })
      .subscribe({
        next: (response : any) => {
          // this.logged = true, this.user.emit(response.username), console.log('Emitted'),
          // this.loading = true;
          // localStorage.setItem('loggedIn', 'true');
          // localStorage.setItem('username', `${response.username}`),
          this.router.navigate([`/`])
        },
        error: (error) => console.log(error),
      });
  }
}