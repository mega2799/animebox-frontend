import { NgFor, AsyncPipe, CommonModule } from '@angular/common';
import { Component, Input, OnInit } from '@angular/core';
import { FormControl, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatButtonModule } from '@angular/material/button';
import { MatDividerModule } from '@angular/material/divider';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { ButtonIOComponent } from '../../button-io/button-io.component';
import { MangaService } from 'src/app/services/manga.service';
import {MatDialog, MatDialogModule} from '@angular/material/dialog';
import { DialogContentManga } from '../dialogs/contentManga.dialog.';
import { AddMangaDialog } from '../dialogs/add-manga.dialog';
import  SimpleDialog  from '../dialogs/simple.dialog';


export interface Volume {
  readonly number: number;
  readonly released: Date;
}

export interface Manga {

  readonly title: string;

  readonly description: string;

  readonly image: string;

  readonly genres: string[];

  readonly status: 'Ongoing' | 'Completed';

  readonly rating: number;

  readonly authors: string[];

  readonly volumes: Volume[];
}

@Component({
  selector: 'app-manga-io',
  templateUrl: '../../button-io/button-io.component.html',
  styleUrls: ['../../button-io/button-io.component.css'],
  // styleUrls: ['./manga-io.component.css'],
  standalone: true,
  imports: [MatButtonModule, MatDividerModule, MatIconModule,
    FormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatAutocompleteModule,
    ReactiveFormsModule,
    NgFor,
    MatSlideToggleModule,
    AsyncPipe,
    CommonModule, 
   MatDialogModule
  ],
})
export class MangaIoComponent extends ButtonIOComponent implements OnInit{
  constructor(private readonly mangaService: MangaService, 
    public dialog: MatDialog
    ) {
    super()
   }

   @Input()
   override addIcon: string = 'note_add'

  override subTitleBar: string = 'rating';

   override label : string = 'Title/code'

  render(){

    this.mangaService.getMangas().subscribe(resp => {
      const r : any[] = resp;
      this.listOfEntities= r.map((entity) => {return {flag: entity.image, name : entity.title, littleTitle: entity.rating}});
    });
    this.stateCtrl.valueChanges.forEach((val) => console.log(val))
  }
  ngOnInit(): void {
    this.render()
  }


  //TODO non va bene unire le cose, quindi l add manga avra' il suo bar in sui scrivere
  // e aggiungere eventualmente la foto
  override addEntity() {
    if(this.stateCtrl.value){
      const selectedManga : string = this.stateCtrl.value ;

      if(this.listOfEntities.filter((entity)=> entity.name === selectedManga).length) {
        this.dialog.open(SimpleDialog, {
        data : {
          message : `There's already !`
        }
      })
      return
    }


      const dialogRef = this.dialog.open(AddMangaDialog)

      dialogRef.afterClosed().subscribe(imageSrc => {
        if(!imageSrc) return;
        this.mangaService.addManga({ title : selectedManga, image : imageSrc}).subscribe((res : any) => {
          // if(!res) console.log('Non si e trovato');
          if(!res)  this.dialog.open(SimpleDialog, {
            data : {
              message: `Supplier does not sell that manga :^O \n Check the code you entered`
            }
          })
          this.render();
        });
        
      })
      
      // this.mangaService.addManga(selectedManga).subscribe((res : any) => {
      //   if(!res) console.log('Non si e trovato');
      // });
    }
}

  override openDetail() {
    if(this.stateCtrl.value){
      const selectedManga : string = this.stateCtrl.value ;

      if(!this.listOfEntities.filter((entity)=> entity.name === selectedManga).length) {
        this.dialog.open(SimpleDialog, {
        data : {
          message : `There's no ${selectedManga}!`
        }
      })
      return
    }
      this.mangaService.getManga(selectedManga).subscribe((res : any) => {
        res.volumes = res.volumes.map((vol : any) => ({number : vol.number, released : new Date(vol.released).toLocaleDateString()}))
        
        this.dialog.open(DialogContentManga, {
          data : {
            res
            // title : res.title
          }
        } );
      });
    }else{
      console.log(`None selected`);
      
    }
  }

}


