import { CommonModule } from '@angular/common';
import { Component, Inject } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MAT_DIALOG_DATA, MatDialogModule } from '@angular/material/dialog';
import { MatDividerModule } from '@angular/material/divider';


@Component({
  selector: 'dialog-content-example-dialog',
  templateUrl: './manga-io.dialog.html',
  standalone: true,
  imports: [MatDialogModule, MatButtonModule, CommonModule, MatDividerModule],
})
export class DialogContentManga {
  constructor(@Inject(MAT_DIALOG_DATA) public data: any) {}

  showOnlyMangaInfo = true;
}
