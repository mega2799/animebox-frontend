import { CommonModule } from '@angular/common';
import { Component, Inject } from '@angular/core';
import { FormControl, FormsModule, ReactiveFormsModule, Validators } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MAT_DIALOG_DATA, MatDialogModule, MatDialogRef } from '@angular/material/dialog';

const reg = '(https?://)?([\\da-z.-]+)\\.([a-z.]{2,6})[/\\w .-]*/?'; //regex to validate URL's

@Component({
  selector: 'dialog-manga-dialog',
  templateUrl: './add.manga.dialog.html',
  standalone: true,
  imports: [MatDialogModule, MatFormFieldModule, MatInputModule, MatDialogModule, FormsModule, MatButtonModule, CommonModule, ReactiveFormsModule],
})
export class AddMangaDialog {
  constructor(
    public dialogRef: MatDialogRef<AddMangaDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any) { }
    
    image = new FormControl('', [Validators.required, Validators.pattern(reg)]);

    getErrorMessage() {
    if (this.image.hasError('required')) {
      return 'You must enter a value';
    }

    return this.image.hasError('pattern') ? 'Not a valid url' : '';
  }
  // onNoClick(): void {
  //   this.dialogRef.close();
  // }
}
