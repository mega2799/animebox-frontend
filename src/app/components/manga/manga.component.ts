import { Component, Input, OnInit } from "@angular/core";
import { HttpClient } from '@angular/common/http';
import { MangaService } from "src/app/services/manga.service";
import { ButtonIOComponent } from "../button-io/button-io.component";
import { MatDialog } from "@angular/material/dialog";
import { DialogContentManga } from "./dialogs/contentManga.dialog.";


@Component({
  selector: 'app-manga',
  templateUrl: './manga.component.html',
  styleUrls: ['./manga.component.css']
})

export class MangaComponent implements OnInit {
  constructor(private readonly mangaService: MangaService, 
    public dialog: MatDialog

    ) {


  }

openDialog(title: any) {
      this.mangaService.getManga(title).subscribe((res : any) => {
        res.volumes = res.volumes.map((vol : any) => ({number : vol.number, released : new Date(vol.released).toLocaleDateString()}))
        this.dialog.open(DialogContentManga, {
          data : {
            res
          }
        } );
      });
}

  mangas: any[] = []
  ngOnInit(): void {
    this.mangaService.getMangas().subscribe(resp => {
      this.mangas = resp;
    });
  }

  onButtonClick(event : any) {
    console.log(MangaComponent.name + ' has a button that has been clicked')
  }

}

