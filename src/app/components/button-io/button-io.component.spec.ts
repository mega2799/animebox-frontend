import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ButtonIOComponent } from './button-io.component';

describe('ButtonIOComponent', () => {
  let component: ButtonIOComponent;
  let fixture: ComponentFixture<ButtonIOComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ButtonIOComponent]
    });
    fixture = TestBed.createComponent(ButtonIOComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
