import { Component, EventEmitter, Input, Output } from '@angular/core';
import { MatIconModule } from '@angular/material/icon';
import { MatDividerModule } from '@angular/material/divider';
import { MatButtonModule } from '@angular/material/button';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { NgFor, AsyncPipe, CommonModule } from '@angular/common';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { FormControl, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Observable, map, startWith } from 'rxjs';
import { trigger, state, style, transition, animate } from '@angular/animations';


@Component({
  selector: 'app-button-io',
  templateUrl: './button-io.component.html',
  styleUrls: ['./button-io.component.css'],
  standalone: true,
  imports: [MatButtonModule, MatDividerModule, MatIconModule,
    FormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatAutocompleteModule,
    ReactiveFormsModule,
    NgFor,
    MatSlideToggleModule,
    AsyncPipe,
    CommonModule,
  ],
  animations : [
      trigger('widthGrow', [
            state('closed', style({
                width: 0,
            })),
            state('open', style({
                width: 400
            })),
            transition('* => *', animate(150))
        ]),
  ]

})
export class ButtonIOComponent {
  filteredStates: Observable<any[]> = new Observable();

  editMode: boolean = false;

  displayAdd = true;

  displayDetail = true;

  @Output() buttonClickEmitter = new EventEmitter<any>();

  @Input()
  stateCtrl = new FormControl('');
  @Input()
  addIcon: string = '';

  @Input()
  label : string = '';

  @Input()
  subTitleBar : string = '';

  @Input()
  listOfEntities: any[] = [];

  genericClick() {
    console.log(`${ButtonIOComponent.name} has been pressed`);
    this.buttonClickEmitter.emit(1);
    this.editMode = !this.editMode;
  }

  constructor() {
    this.filteredStates = this.stateCtrl.valueChanges.pipe(
      startWith(''),
      map(state => (state ? this._filterStates(state) : this.listOfEntities.slice())),
    );

  }

  private _filterStates(value: string): any[] {
    const filterValue = value.toLowerCase();
    return this.listOfEntities.filter(state => state.name.toLowerCase().includes(filterValue));
  }


  openDetail() {
    //TO EXTEND
  }

  addEntity() {

    //TO EXTEND
  }
}
