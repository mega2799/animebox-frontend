import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { LoginComponent } from '../auth/login/login.component';
import { HomeComponent } from '../home/home.component';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor(
    private router : Router
  ){}
  @Input() userName : string = '';

  interaction : string = 'Register';

  ngOnInit(): void {
    this.userName = localStorage.getItem('username') || ''
    localStorage.setItem('format', 'Register')
  }

  logged : boolean = JSON.parse(localStorage.getItem('loggedIn') || 'false');

  logout(){
    localStorage.removeItem('loggedIn')
    localStorage.removeItem('username')
    window.location.reload()
    // this.router.navigate(['/'])
  }
  
  switchLogReg(){
    this.interaction = this.interaction === 'Register' ? 'Login' : 'Register'
    localStorage.setItem('format', this.interaction)
  }
}
