import { animate, style, transition, trigger, useAnimation } from '@angular/animations';
import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { MangaService } from 'src/app/services/manga.service';
import { scaleIn, scaleOut } from './home.animation';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  animations: [
    trigger("slideAnimation", [
      /* scale */
      transition("void => *", [useAnimation(scaleIn, {params: { time: '500ms' }} )]),
      transition("* => void", [useAnimation(scaleOut, {params: { time: '500ms' }})]),
    ])
  ]
})
export class HomeComponent implements OnInit{
  constructor(private readonly mangaService: MangaService){}
  mangasImages : any = [] 
  currentSlide = [0, 1, 2]
  userName : any;
  @Output() user = new EventEmitter<any>();
  ngOnInit(): void {
    this.mangaService.getMangas().subscribe((res : any[]) => {
    this.mangasImages = res.map((singleManga) => singleManga.image);
    // setInterval(() => this.onPreviousClick(), 1500);
    this.userName = localStorage.getItem('loggedIn')? localStorage.getItem('username') : ''
    setInterval(() => this.onNextClick(), 4000)
    })
  }
  onPreviousClick() {
    this.currentSlide = this.currentSlide.map((val) => {
    const previous = val - 1;
    return previous < 0 ? this.mangasImages.length - 1 : previous;
    })
    // const previous = this.currentSlide - 1;
    // this.currentSlide = previous <= 0 ? this.mangasImages.length - 1 : previous;
    // console.log("previous clicked, new current slide is: ", this.currentSlide);
  }

  onNextClick() {
    this. currentSlide = this.currentSlide.map((val) => {
      const next = val + 1;
      return next === this.mangasImages.length - 1 ? 0 : next;
    })
    // const next = this.currentSlide + 1;
    // this.currentSlide = next > this.mangasImages.length - 1 ? 0 : next;
    // console.log("next clicked, new current slide is: ", this.currentSlide);
  } 

  onLoginEmitter(user : any) {
    this.userName = user;
    this.user.emit(user)
  }
}
