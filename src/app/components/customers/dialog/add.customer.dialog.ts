import { CommonModule } from "@angular/common";
import { Component, Inject } from "@angular/core";
import { FormControl, FormGroup, FormsModule, ReactiveFormsModule, Validators } from "@angular/forms";
import { MatButtonModule } from "@angular/material/button";
import { MatDialogModule, MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatInputModule } from "@angular/material/input";

@Component({
    selector: 'dialog-customer-dialog',
    templateUrl: './add.customer.dialog.html',
    standalone: true,
    imports: [MatDialogModule, MatFormFieldModule,ReactiveFormsModule ,MatInputModule, MatDialogModule, FormsModule, MatButtonModule, CommonModule],
  })
  export class AddCustomerDialog {
    constructor(
      public dialogRef: MatDialogRef<AddCustomerDialog >,
      @Inject(MAT_DIALOG_DATA) public data: any) { }

    customer = {
      CF : '',
      name : '',
      surname: '',
      email : '',
      telegramID : ''
    }

  customerForm: FormGroup = new FormGroup({
    CF: new FormControl('', [Validators.required]),
    name: new FormControl(this.data.name, [Validators.required]),
    surname: new FormControl(this.data.surname, [Validators.required]),
    email: new FormControl(this.data.name.toLowerCase() + '.' + this.data.surname.toLowerCase() + '@', [Validators.required, Validators.email]),
    telegramID: new FormControl('/t.me/' + this.data.name.toLowerCase(), [Validators.required]),
  });
  
    onNoClick(): void {
      this.data = undefined;
      this.dialogRef.close();
    }

  onSubmit() {
    console.log(this.customerForm.value);
    
    this.data = this.customerForm.value;
  }

    getErrorMessage(item : string) {
      switch (item) {
        case 'CF':
          return 'You must enter a value for CF';
          break;
        case 'name':
          return 'name is required';
          break;
        case 'surname':
          return 'surname is required';
          break;
        case 'email':
          if(this.customerForm.get('email')?.hasError('email')){
            return 'Email is malformed'
          }
          return 'email is required';
          break;
        case 'telegramID':
          return 'telegramID is required';
          break;
        default:
          return '';
          break;
      }
    // if (this.customerForm.get('CF')?.hasError('required')) {
    // }
    // if (this.customerForm.get('name')?.hasError('required')) {
    //   return 'You must enter a value for name';
    // }
    // if (this.customerForm.get('surname')?.hasError('required')) {
    //   return 'You must enter a value for surname';
    // }
    // if (this.customerForm.get('email')?.hasError('required')) {
    //   return 'You must enter a mail';
    // }
    // if (this.customerForm.get('telegramID')?.hasError('required')) {
    //   return 'You must enter a value for telegram';
    // }
    // return '';
  }
}
  