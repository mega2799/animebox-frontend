import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomerIOComponent } from './customer-io.component';

describe('CustomerIOComponent', () => {
  let component: CustomerIOComponent;
  let fixture: ComponentFixture<CustomerIOComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [CustomerIOComponent]
    });
    fixture = TestBed.createComponent(CustomerIOComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
