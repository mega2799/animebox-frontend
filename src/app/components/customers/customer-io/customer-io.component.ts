import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ButtonIOComponent } from '../../button-io/button-io.component';
import { MatIconModule } from '@angular/material/icon';
import { MatDividerModule } from '@angular/material/divider';
import { MatButtonModule } from '@angular/material/button';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgFor, AsyncPipe, CommonModule } from '@angular/common';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { CustomerService } from 'src/app/services/customer.service';
import { MatDialog } from '@angular/material/dialog';
import SimpleDialog from '../../manga/dialogs/simple.dialog';
import { AddCustomerDialog } from '../dialog/add.customer.dialog';
import { Router } from '@angular/router';

@Component({
  selector: 'app-customer-io',
  // templateUrl: './customer-io.component.html',
  templateUrl: '../../button-io/button-io.component.html',
  standalone: true,
  styleUrls: ['../../button-io/button-io.component.css'],
  // styleUrls: ['./customer-io.component.css'],
  imports: [MatButtonModule, MatDividerModule, MatIconModule,
    FormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatAutocompleteModule,
    ReactiveFormsModule,
    NgFor,
    MatSlideToggleModule,
    AsyncPipe,
    CommonModule
  ],
})
export class CustomerIOComponent extends ButtonIOComponent implements OnInit {
  constructor(
    private readonly customerService : CustomerService,
    public dialog: MatDialog,
    private router : Router
  ){
    super()
  } 

  @Output() override buttonClickEmitter = new EventEmitter<any>();

  @Input()
  override addIcon: string = 'add';

  @Input()
  override listOfEntities: any[] = [];
  override displayDetail: boolean = true;
  override label: string = 'Name/Surname'
  override subTitleBar: string = 'email'
  
  renderCustomers() {
    this.customerService.getCustomers().subscribe(res => {
      const cust : any[] = res;
      this.listOfEntities =  cust.map((entity) => { return {flag : entity.image, name : entity.name, littleTitle: entity.email, CF : entity.CF}})
    })
  }
  ngOnInit(): void {
    this.renderCustomers()
  }

   override addEntity() {
    if(this.stateCtrl.value){
      let selectedCustomer : string = this.stateCtrl.value ;
      let surname;
      //nome e cognome 
      if(selectedCustomer.includes(' ')){
        surname = selectedCustomer.split(' ')[1]
        selectedCustomer = selectedCustomer.split(' ')[0]
      }
      console.log(selectedCustomer, surname);
      

      if(this.listOfEntities.filter((entity)=> entity.name === selectedCustomer).length) {
        this.dialog.open(SimpleDialog, {
        data : {
          message : `There's already !`
        }
      })
      return
    }


     const dialogRef = this.dialog.open(AddCustomerDialog, {
      data :{
        CF: '',
        name : selectedCustomer,
        surname: surname,
        email : '',
        telegramID: ''
      }
      // data :{
      //   cf : '',
      //   name : '',
      //   surname: '',
      //   image: '',
      //   email : '',
      // }
     })

      dialogRef.afterClosed().subscribe(resp=> {
        console.log(resp);
        if(!resp) return;
        this.customerService.addCustomer(resp).subscribe((res) => {
          if(!res) console.log('Non si e trovato');
          // window.location.reload();
          this.renderCustomers();
          this.buttonClickEmitter.emit(true);
        })
      })
    }

  }

  override openDetail(){
    if(this.stateCtrl.value){
    const customer : any = this.listOfEntities.find((entity)=> entity.name === this.stateCtrl.value);
    this.router.navigate([`/box/${customer.CF}`])
      // this.customerService.getCustomer(this.stateCtrl.value).subscribe(res => {
      //   console.log(res);
        
      // })
    }

  }
}
