import { Component, Input, OnInit } from '@angular/core';
import { MatDividerModule } from '@angular/material/divider';
import { Router } from '@angular/router';
import { CustomerService } from 'src/app/services/customer.service';

@Component({
  selector: 'app-customers',
  templateUrl: './customers.component.html',
  styleUrls: ['./customers.component.css'],
})
export class CustomersComponent implements OnInit{
  constructor(
    private readonly customerService : CustomerService,
    private router : Router
  ){} 

  @Input()
  // addIcon : string = 'person_add'
  addIcon : string = 'add'

  onButtonClick(event : any) {
    console.log(CustomersComponent.name + ' has a button that has been clicked')
    this.customerService.getCustomers().subscribe(res => {
      this.customers = res
    })

  }

  customers : any[] = []

  ngOnInit(): void {
    this.customerService.getCustomers().subscribe(res => {
      this.customers = res
    })
    
  }


openBox(CF: string) {
  this.router.navigate([`/box/${CF}`])
}
}
