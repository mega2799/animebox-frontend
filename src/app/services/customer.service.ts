import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

export interface ResponseModel{
    success:boolean;
    message:string;

}

export interface ListResponseModel<T> extends ResponseModel{
    data:T[];
}

@Injectable({
  providedIn: 'root'
})
export class CustomerService {
  constructor(private httpService : HttpClient) {
  }

  updateCustomer(value: any) {
    const url = environment.API +'customer/'

    return this.httpService.patch<ListResponseModel<any>>(url + value._id,
      {customer : value} ,
      {
      headers : {
        'Acess-Control-Allow-Origin' : '*',
        'Access-Control-Allow-Headers' : 'Origin, X-Requested-With, Content-Type, Accept',
        'Content-Type' : 'application/json'
      }
    });

  }
  addCustomer(resp: any) {
    const url = environment.API +'customer'

    return this.httpService.post<ListResponseModel<any>>(url,
      resp,
      {
      headers : {
        'Acess-Control-Allow-Origin' : '*',
        'Access-Control-Allow-Headers' : 'Origin, X-Requested-With, Content-Type, Accept',
        'Content-Type' : 'application/json'
      }
    });
  }
  public getCustomers() : Observable<any>{
    const url = environment.API +'customer'
    return this.httpService.get<ListResponseModel<any>>(url, {
      headers : {
        'Acess-Control-Allow-Origin' : '*',
        'Access-Control-Allow-Headers' : 'Origin, X-Requested-With, Content-Type, Accept',
        'Content-Type' : 'application/json'
      }
    });
  }

  
  public getCustomer(CF : string) : Observable<any>{
    const url = environment.API +'customer/'
    return this.httpService.get<ListResponseModel<any>>(url + CF, {
      headers : {
        'Acess-Control-Allow-Origin' : '*',
        'Access-Control-Allow-Headers' : 'Origin, X-Requested-With, Content-Type, Accept',
        'Content-Type' : 'application/json'
      }
    });
  }
}
