import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

export interface ResponseModel{
    success:boolean;
    message:string;

}

export interface ListResponseModel<T> extends ResponseModel{
    data:T[];
}

@Injectable({
  providedIn: 'root'
})
export class MangaService {
  addManga(selectedManga: {title : string, image: string}) {
    const url = environment.API +'product'
    console.log(`sending`);
    console.log(selectedManga);
    
    return this.httpService.post<ListResponseModel<any>>(url,
      selectedManga,
      {
      headers: {
        'Acess-Control-Allow-Origin': '*',
        'Access-Control-Allow-Headers': 'Origin, X-Requested-With, Content-Type, Accept',
        'Content-Type': 'application/json'
      }
    });


  }
  public getManga(mangaTitle : string) {
    const url = environment.API +'product'

    return this.httpService.get<ListResponseModel<any>>(url + `/${mangaTitle}`, {
      params : {
        title : mangaTitle
      },
      headers : {
        'Acess-Control-Allow-Origin' : '*',
        'Access-Control-Allow-Headers' : 'Origin, X-Requested-With, Content-Type, Accept',
        'Content-Type' : 'application/json'
      }
    });

  }

  constructor(private httpService : HttpClient) {
  }


  public getMangas() : Observable<any>{
    const url = environment.API +'product'

    console.log('calling ', url)
    return this.httpService.get<ListResponseModel<any>>(url, {
      headers : {
        'Acess-Control-Allow-Origin' : '*',
        'Access-Control-Allow-Headers' : 'Origin, X-Requested-With, Content-Type, Accept',
        'Content-Type' : 'application/json'
      }
    });
  }
}
