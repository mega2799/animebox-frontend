import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

export interface ResponseModel{
    success:boolean;
    message:string;

}

export interface ListResponseModel<T> extends ResponseModel{
    data:T[];
}

@Injectable({
  providedIn: 'root'
})
export class BoxService {

  constructor(private httpService : HttpClient) {
  }

  deleteVolume(boxId: string, title: number, current : number) {
    const url = environment.API +'box/delete'
    
    return this.httpService.post<ListResponseModel<any>>(url + boxId,
      {
        title : title,
        currentID : current
      },
     {
      headers : {
        'Acess-Control-Allow-Origin' : '*',
        'Access-Control-Allow-Headers' : 'Origin, X-Requested-With, Content-Type, Accept',
        'Content-Type' : 'application/json'
      }
    });
  }

  increaseStatus(boxId: string, title: number) {
    const url = environment.API +'box/update'
    
    return this.httpService.post<ListResponseModel<any>>(url + boxId,
      {
        title : title
      },
     {
      headers : {
        'Acess-Control-Allow-Origin' : '*',
        'Access-Control-Allow-Headers' : 'Origin, X-Requested-With, Content-Type, Accept',
        'Content-Type' : 'application/json'
      }
    });

  }

  updateBox(_id: any, customerProd : any) {
    const url = environment.API +'box/'
    
    return this.httpService.post<ListResponseModel<any>>(url + _id,
      {
        customerProduct: customerProd
      },
     {
      headers : {
        'Acess-Control-Allow-Origin' : '*',
        'Access-Control-Allow-Headers' : 'Origin, X-Requested-With, Content-Type, Accept',
        'Content-Type' : 'application/json'
      }
    });

  }

  createBox(_id: any) {
    const url = environment.API +'box'
    return this.httpService.post<ListResponseModel<any>>(url,
 {
      customer : {id : _id},
      product: undefined,
    },
     {
      
      headers : {
        'Acess-Control-Allow-Origin' : '*',
        'Access-Control-Allow-Headers' : 'Origin, X-Requested-With, Content-Type, Accept',
        'Content-Type' : 'application/json'
      }
    });

  }

  public getPersonalBox(id :string) : Observable<any>{
    const url = environment.API +'box/'
    return this.httpService.get<ListResponseModel<any>>(url + id, {
      headers : {
        'Acess-Control-Allow-Origin' : '*',
        'Access-Control-Allow-Headers' : 'Origin, X-Requested-With, Content-Type, Accept',
        'Content-Type' : 'application/json'
      }
    });
  }

}
